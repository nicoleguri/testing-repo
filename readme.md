1. git branch   
2. git status   
3. git diff   
4. git show   
5. git checkout -b {name of branch}   
6. git -d {branch to be deleted}   
   
7. git init   
8. git remote add origin{link}   
9. git add .   
10. git commit -m "commit msg"   
11. git push origin{branch name}   
   
12. git commit --amend --no-edit 
